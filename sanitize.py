#### STL / External
# Typing
from collections.abc import Callable

# Data Management
import re

# Logging
import logging
import datetime

#### Local
import constants


def log_and_const(
    log: Callable[[re.Match], None], repl: str
) -> Callable[[re.Match], str]:
    def helper(caughtValue: re.Match) -> str:
        log(caughtValue)
        return repl

    return helper


def format_matched(regexName: str, match: re.Match) -> str:
    return f"Match on '{regexName}' regex: {{{match[0]}}}"


def batch(texts: list[str]) -> None:
    logger = logging.getLogger(__name__)
    logging.basicConfig(filename=constants.filename, level=logging.INFO)
    logger.info("New Batch at: " + str(datetime.datetime.now()))

    compiledRegexes = {
        name: re.compile(regex, re.IGNORECASE)
        for name, regex in constants.regexes.items()
    }

    for text in texts:
        for name, regex in compiledRegexes.items():
            log_matched = lambda match: logger.info(
                format_matched(name, match)
            )
            regex.sub(
                log_and_const(log_matched, ""),
                text,
            )


def main() -> None:
    with open("test_examples.txt") as file:
        batch(file.readlines())


if __name__ == "__main__":
    main()
